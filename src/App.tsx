import React, { ReactElement } from 'react'
import './App.css'
import 'bootstrap/dist/css/bootstrap-grid.css'
import { Home } from 'Components/Home/Home'

function App(): ReactElement {
  return <Home />
}

export default App
