import { Indicator } from './Indicator'
import Renderer from 'react-test-renderer'

describe('Indicator component', () => {
  const mockCallBack = jest.fn()

  beforeEach(() => {
    mockCallBack.mockClear()
  })

  it('when it is used', () => {
    const render = Renderer.create(<Indicator />)
    const tree = render.toJSON()

    expect(tree).toMatchSnapshot()
  })
})
