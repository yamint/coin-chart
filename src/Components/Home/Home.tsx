import { FunctionComponent } from 'react'
import styled from 'styled-components'
import logo from 'assets/logo.png'
import { Layout, Row } from 'antd'
import { useAppSelector } from 'store/hooks'
import { selectData } from 'store/exchange'
import { Indicator } from 'Components/UiKit/Indicator'
import { TopBar } from './TopBar/TopBar'
import { Rates } from './Rates/Rates'

const { Content, Header } = Layout

export const Home: FunctionComponent = () => {
  const { is_fetching } = useAppSelector(selectData)

  return (
    <Layout>
      {is_fetching && <Indicator />}
      <TopHeader>
        <Row>
          <img width={150} alt="wisdom tree" src={logo} />
        </Row>
      </TopHeader>
      <MainContent>
        <TopBar />
        <TableContainer>
          <Rates />
        </TableContainer>
      </MainContent>
    </Layout>
  )
}
const TopHeader = styled(Header)`
  width: '100%';
  background: white;
  padding: 20px;
  border-bottom: 1px solid #f5f5f5;
  height: 100%;
`
const MainContent = styled(Content)`
  padding: 20px;
`
const TableContainer = styled.div`
  max-width: 600px;
  margin: 0 auto;
`
