import React from 'react'
import { TopBar } from './TopBar'
import { Provider } from 'react-redux'
import Renderer from 'react-test-renderer'
import { store } from 'store'

it('Should render Home component', () => {
  const render = Renderer.create(
    <Provider store={store}>
      <TopBar />
    </Provider>
  )
  const tree = render.toJSON()

  expect(tree).toMatchSnapshot()
})
