import { Col, InputNumber, Row, Select, Space } from 'antd'
import { supportedCurrency, supportedCurrencyTypes } from 'api/coinbase.interface'
import { MAX_PRECISION, MAX_REFRESH_TIME, MIN_PRECISION, MIN_REFRESH_TIME } from 'constants/index'
import { FunctionComponent, useCallback } from 'react'
import { selectData, setCurrency, setDecimals, setInterval } from 'store/exchange'
import { useAppDispatch, useAppSelector } from 'store/hooks'
import styled from 'styled-components'

const { Option } = Select

export const TopBar: FunctionComponent = () => {
  const { currency, interval, decimals } = useAppSelector(selectData)
  const dispatch = useAppDispatch()

  const setNewCurrency = useCallback(
    currency => {
      dispatch(setCurrency(currency))
    },
    [dispatch]
  )

  const changeInterval = useCallback(
    (value: number) => {
      dispatch(setInterval(value))
    },
    [dispatch]
  )

  const changeDecimals = useCallback(
    (value: number) => {
      dispatch(setDecimals(value))
    },
    [dispatch]
  )

  return (
    <Container gutter={[16, 20]} justify="center">
      <Col>
        <Select
          showSearch
          style={{ width: '100px' }}
          placeholder="Select currency"
          optionFilterProp="children"
          value={currency}
          onChange={setNewCurrency}
          filterOption={(input: any, option: any) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
          }
        >
          {supportedCurrency.map((currency: supportedCurrencyTypes) => (
            <Option key={currency} value={currency}>
              {currency}
            </Option>
          ))}
        </Select>
      </Col>
      <Col>
        <Space>
          Update interval
          <InputNumber
            min={MIN_REFRESH_TIME}
            style={{ width: '200px' }}
            max={MAX_REFRESH_TIME}
            addonAfter="Seconds"
            onChange={changeInterval}
            defaultValue={interval}
          />
        </Space>
      </Col>
      <Col>
        <Space>
          Decimals precision:
          <InputNumber
            style={{ width: '100px' }}
            min={MIN_PRECISION}
            max={MAX_PRECISION}
            onChange={changeDecimals}
            defaultValue={decimals}
          />
        </Space>
      </Col>
    </Container>
  )
}
const Container = styled(Row)`
  margin: 20px;
`
