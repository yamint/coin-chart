import { FunctionComponent, useEffect } from 'react'
import styled from 'styled-components'
import { Col, Row, RowProps, Table } from 'antd'
import { useAppDispatch, useAppSelector, useInterval } from 'store/hooks'
import { Changes, ExchangeRatesProcessedData, selectData } from 'store/exchange'
import { GreenColor, RedColor } from 'constants/index'
import { getCoinRatesRequestAsync } from 'store/exchange/asyncActions'
import { CaretUpOutlined, CaretDownOutlined, LineOutlined } from '@ant-design/icons'

interface RateInterface {
  color?: string
}

export const Rates: FunctionComponent = () => {
  const { rates, currency, interval, decimals } = useAppSelector(selectData)
  const dispatch = useAppDispatch()

  useInterval(() => {
    dispatch(getCoinRatesRequestAsync(currency))
  }, interval)

  useEffect(() => {
    dispatch(getCoinRatesRequestAsync(currency))
  }, [dispatch, currency])

  const columns = [
    {
      title: 'Coin',
      dataIndex: 'coin',
      key: 'coin',
    },
    {
      title: 'Rate',
      dataIndex: 'rate',
      key: 'rate',
      render: (rawRate: number, record: ExchangeRatesProcessedData) => {
        const rate = rawRate.toFixed(decimals)
        if (record.changes === Changes.INCREASED)
          return (
            <Rate justify="start" color={GreenColor} gutter={{ xs: 5, md: 16 }}>
              <Col>
                <CaretUpOutlined />
              </Col>
              <Col>{rate}</Col>
              <Col>{currency}</Col>
            </Rate>
          )
        if (record.changes === Changes.DECREASED)
          return (
            <Rate justify="start" color={RedColor} gutter={{ xs: 5, md: 16 }}>
              <Col>
                <CaretDownOutlined />
              </Col>
              <Col>{rate}</Col>
              <Col>{currency}</Col>
            </Rate>
          )
        return (
          <Rate justify="start" gutter={{ xs: 5, md: 16 }}>
            <Col>
              <LineOutlined />
            </Col>
            <Col> {rate}</Col>
            <Col> {currency}</Col>
          </Rate>
        )
      },
      width: '100%',
    },
  ]

  return <Table dataSource={rates} columns={columns} />
}
const Rate = styled(Row)<RateInterface & RowProps>`
  color: ${props => props.color};
  white-space: wrap;
  max-width: 40vw;
  word-break: break-all;
`
