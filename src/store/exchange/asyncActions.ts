import { AsyncThunk, createAsyncThunk } from '@reduxjs/toolkit'
import { ExchangeResponse, supportedCurrencyTypes } from 'api/coinbase.interface'
import { CoinBaseService } from 'api/index'

export const getCoinRatesRequestAsync: AsyncThunk<ExchangeResponse, supportedCurrencyTypes, any> = createAsyncThunk(
  'coinbase/exchange/rates',
  async (currency: supportedCurrencyTypes = 'USD') => {
    return await CoinBaseService.exchangeRate(currency)
  }
)
