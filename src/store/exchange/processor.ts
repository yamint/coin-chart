import { Rates } from 'api/coinbase.interface'
import { SET_TOP_COINS, TOP_COINS } from 'constants/index'
import { Changes, ExchangeRatesProcessedData } from '.'

export const processor = (rates: Rates, previousState: ExchangeRatesProcessedData[]): ExchangeRatesProcessedData[] => {
  try {
    if (!rates || !(rates instanceof Object)) return []
    const coinsList = Object.entries(rates)
    const unsorted = coinsList.map(([coin, rateString]) => {
      const rate = Math.pow(+rateString, -1)
      let changes: Changes = Changes.SAME
      if (previousState?.length > 0) {
        const previousValue = previousState.find(coins => coins.coin === coin)
        const difference = rate - previousValue.rate
        if (difference > 0) changes = Changes.INCREASED
        if (difference < 0) changes = Changes.DECREASED
      }
      return {
        coin,
        rate,
        changes,
      }
    })
    if (SET_TOP_COINS) {
      return unsorted.sort((a: ExchangeRatesProcessedData) => (TOP_COINS.includes(a.coin) ? -1 : 0))
    } else {
      return unsorted
    }
  } catch (e) {
    return []
  }
}
