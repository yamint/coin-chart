import getBicyclesReducer, { ExchangeRateState } from './index'

import { getCoinRatesRequestAsync } from './asyncActions'
import { DEFAULT_DECIMALS, DEFAULT_INTERVAL } from 'constants/index'

describe('exchange rate reducer', () => {
  const initialState: ExchangeRateState = {
    is_fetching: true,
    rates: [],
    currency: 'USD',
    decimals: DEFAULT_DECIMALS,
    interval: DEFAULT_INTERVAL,
  }

  it('should handle initial state', () => {
    expect(getBicyclesReducer(undefined, { type: 'unknown' })).toEqual({
      is_fetching: false,
      rates: [],
      currency: 'USD',
      decimals: DEFAULT_DECIMALS,
      interval: DEFAULT_INTERVAL,
    })
  })

  it('should handle getCoinRatesRequestAsync.pending', () => {
    const actual = getBicyclesReducer(initialState, getCoinRatesRequestAsync.pending)
    expect(actual.is_fetching).toEqual(true)
  })

  it('should handle getCoinRatesRequestAsync.fulfilled', async () => {
    const actual = getBicyclesReducer(initialState, getCoinRatesRequestAsync.fulfilled)
    expect(actual.rates).not.toBeNull()
    expect(actual.currency).not.toBeNull()
  })

  it('should handle getCoinRatesRequestAsync.rejected', () => {
    const actual = getBicyclesReducer(initialState, getCoinRatesRequestAsync.rejected)
    expect(actual.is_fetching).toEqual(false)
  })
})
