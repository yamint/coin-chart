import { Rates } from 'api/coinbase.interface'
import { Changes, ExchangeRatesProcessedData } from '.'
import { processor } from './processor'
// BTC: "2.4010670341899938e-05",

describe('Test processor function', () => {
  it('should handle new values', () => {
    const previousState: ExchangeRatesProcessedData[] = []
    const rates: Rates = {
      BTRST: '0.2710027100271003',
      ALL: '1258.82',
      AMD: '4769.49',
      ANG: '17.88',
    }
    const result = processor(rates, previousState)
    expect(result.length).toBe(4)
    const nonSameCoins = result.find(coin => coin.changes !== Changes.SAME)
    const nonNumberCoins = result.find(coin => typeof coin.rate !== 'number')
    expect(nonSameCoins).toBeUndefined()
    expect(nonNumberCoins).toBeUndefined()
  })

  it('should handle changes: DECREASED rates', () => {
    const previousState: ExchangeRatesProcessedData[] = [
      { changes: Changes.SAME, coin: 'AED', rate: 136.73 },
      { changes: Changes.SAME, coin: 'AFN', rate: 5289.225 },
      { changes: Changes.SAME, coin: 'ALL', rate: 258.82 },
      { changes: Changes.SAME, coin: 'AMD', rate: 34769.49 },
      { changes: Changes.SAME, coin: 'ANG', rate: 97.882 },
    ]
    const rates: Rates = {
      AED: '36.73',
      AFN: '589.50',
      ALL: '1258.82',
      AMD: '4769.49',
      ANG: '17.88',
    }
    const result = processor(rates, previousState)
    const mockRates: ExchangeRatesProcessedData[] = [
      { changes: Changes.DECREASED, coin: 'AED', rate: 0.027225701061802342 },
      { changes: Changes.DECREASED, coin: 'AFN', rate: 0.0016963528413910093 },
      { changes: Changes.DECREASED, coin: 'ALL', rate: 0.0007943947506394878 },
      { changes: Changes.DECREASED, coin: 'AMD', rate: 0.0002096660229919761 },
      { changes: Changes.DECREASED, coin: 'ANG', rate: 0.055928411633109625 },
    ]
    expect(result).toStrictEqual(mockRates)
  })

  it('should handle changes: INCREASED rates', () => {
    const previousState: ExchangeRatesProcessedData[] = [
      { changes: Changes.DECREASED, coin: 'AED', rate: 0.027225701061802342 },
      { changes: Changes.DECREASED, coin: 'AFN', rate: 0.0016963528413910093 },
      { changes: Changes.DECREASED, coin: 'ALL', rate: 0.0007943947506394878 },
      { changes: Changes.DECREASED, coin: 'AMD', rate: 0.0002096660229919761 },
      { changes: Changes.DECREASED, coin: 'ANG', rate: 0.055928411633109625 },
    ]
    const rates: Rates = {
      AED: '20.73',
      AFN: '193.50',
      ALL: '268.82',
      AMD: '789.49',
      ANG: '12.88',
    }
    const result = processor(rates, previousState)
    const mockRates: ExchangeRatesProcessedData[] = [
      { changes: Changes.INCREASED, coin: 'AED', rate: 0.048239266763145196 },
      { changes: Changes.INCREASED, coin: 'AFN', rate: 0.00516795865633075 },
      { changes: Changes.INCREASED, coin: 'ALL', rate: 0.0037199613124023513 },
      { changes: Changes.INCREASED, coin: 'AMD', rate: 0.0012666404894298852 },
      { changes: Changes.INCREASED, coin: 'ANG', rate: 0.07763975155279502 },
    ]
    expect(result).toStrictEqual(mockRates)
  })

  it('should handle big changes: INCREASED', () => {
    const previousState: ExchangeRatesProcessedData[] = [{ changes: Changes.SAME, coin: 'BTC', rate: 40648.15 }]
    const rates: Rates = {
      BTC: '2.4010670341899938e-05',
    }
    const result = processor(rates, previousState)
    const mockRates: ExchangeRatesProcessedData[] = [{ changes: Changes.INCREASED, coin: 'BTC', rate: 41648.15 }]
    expect(result).toStrictEqual(mockRates)
  })

  it('should handle big changes: DECREASED', () => {
    const previousState: ExchangeRatesProcessedData[] = [{ changes: Changes.SAME, coin: 'BTC', rate: 40648.15 }]
    const rates: Rates = {
      BTC: '2.9010670341899938e-05',
    }
    const result = processor(rates, previousState)
    const mockRates: ExchangeRatesProcessedData[] = [
      { changes: Changes.DECREASED, coin: 'BTC', rate: 34470.075603723846 },
    ]
    expect(result).toStrictEqual(mockRates)
  })
})
