import { createSlice } from '@reduxjs/toolkit'
import { supportedCurrencyTypes } from 'api/coinbase.interface'
import { DEFAULT_CURRENCY, DEFAULT_DECIMALS, DEFAULT_INTERVAL } from 'constants/index'
import { RootState } from '../index'
import { getCoinRatesRequestAsync } from './asyncActions'
import { processor } from './processor'

export enum Changes {
  SAME = 'SAME',
  INCREASED = 'INCREASED',
  DECREASED = 'DECREASED',
}

export interface ExchangeRatesProcessedData {
  coin: string
  rate: number
  changes: Changes
}

export interface ExchangeRateState {
  is_fetching: boolean
  rates: ExchangeRatesProcessedData[]
  currency: supportedCurrencyTypes
  interval: number
  decimals: number
}

const initialState: ExchangeRateState = {
  is_fetching: false,
  rates: [],
  currency: DEFAULT_CURRENCY,
  interval: DEFAULT_INTERVAL,
  decimals: DEFAULT_DECIMALS,
}

export const exchangeRateSlice = createSlice({
  name: 'exchangeRate',
  initialState,
  reducers: {
    setCurrency: (state, action) => {
      state.currency = action.payload
    },
    setInterval: (state, action) => {
      state.interval = action.payload
    },
    setDecimals: (state, action) => {
      state.decimals = action.payload
    },
  },

  extraReducers: builder => {
    builder
      .addCase(getCoinRatesRequestAsync.pending, state => {
        state.is_fetching = true
      })
      .addCase(getCoinRatesRequestAsync.fulfilled, (state, { payload }) => {
        state.is_fetching = false
        state.rates = processor(payload?.data?.rates, state.rates)
        state.currency = payload?.data?.currency
      })
      .addCase(getCoinRatesRequestAsync.rejected, state => {
        state.is_fetching = false
      })
  },
})

export const selectData = (state: RootState): ExchangeRateState => state.exchange
export const { setCurrency, setInterval, setDecimals } = exchangeRateSlice.actions

export default exchangeRateSlice.reducer
