import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux'
import type { RootState, AppDispatch } from './index'
import { ThunkDispatch } from '@reduxjs/toolkit'
import { useEffect, useLayoutEffect, useRef } from 'react'

// Use throughout your app instead of plain `useDispatch` and `useSelector`
export const useAppDispatch = (): ThunkDispatch<RootState, unknown, any> => useDispatch<AppDispatch>()
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector

export const useInterval = (callback: () => void, delay: number | null): void => {
  const savedCallback = useRef(callback)
  useLayoutEffect(() => {
    savedCallback.current = callback
  }, [callback])
  useEffect(() => {
    if (!delay && delay !== 0) {
      return
    }
    const id = setInterval(() => savedCallback.current(), delay * 1000)
    return () => clearInterval(id)
  }, [delay])
}
