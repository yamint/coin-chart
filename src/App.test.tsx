import React from 'react'
import App from './App'
import { Provider } from 'react-redux'
import Renderer from 'react-test-renderer'
import { store } from 'store'

it('Should render App component', () => {
  const render = Renderer.create(
    <Provider store={store}>
      <App />
    </Provider>
  )
  const tree = render.toJSON()

  expect(tree).toMatchSnapshot()
})
