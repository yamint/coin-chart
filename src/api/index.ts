import { request } from './service/request'
import { ExchangeResponse, supportedCurrencyTypes } from './coinbase.interface'

export const baseUrl = 'https://api.coinbase.com/v2/'
export class CoinBaseService {
  public static async exchangeRate(currency?: supportedCurrencyTypes): Promise<ExchangeResponse> {
    const result = await request({
      method: 'GET',
      path: `exchange-rates`,
      query: {
        currency: currency,
      },
      errors: {
        401: `Unauthorized`,
        403: `Forbidden`,
        404: `Not Found`,
      },
    })
    return result.body
  }
}
