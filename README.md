# Coin rates task
 This application aims to show cryptocurrency rates in a table with auto-update. The API is provided by coinbase.
 The coinbase API did not provide a live connection, so an interval update is used to retrieve information. This time is configurable by users.

## How to run the project
 Clone down this repository. You will need `node` and `yarn` installed globally on your machine.  

Installation:

`yarn install`  

To Run Test Suite:  

`yarn test`  

To Start Server:

`yarn start`  

To Visit App:

`localhost:3000`  


## Main features
 - List all coins based on the selected currency
 - Show if the prices went higher or lower based on the previous value
 - Responsive view
 - Pagination is available
 - Famous coins (BTC, ETH, DOGE) are listed above.
 - Selectable update interval
 - Selectable decimal precision  

## Configs

The below configs are provided in src/constants/index.ts :


- **GreenColor** : Default color for the color green. **RedColor** : Default color for the color red.

-  **DEFAULT_INTERVAL** : Default interval to call exchange API.

-  **TOP_COINS** : List of coins that gets to the top of rates list.

-  **SET_TOP_COINS** : a switch to disable showing top coins in the rates list.

-  **DEFAULT_CURRENCY** : default currency to show rates.

-  **DEFAULT_DECIMALS** : default decimal precision .

-  **MIN_REFRESH_TIME** : Maximum refresh time possible for users to select.

-  **MAX_REFRESH_TIME** : Minimum refresh time possible for users to select.

-  **MIN_PRECISION** : Maximum decimal precision possible for users to select.

-  **MAX_PRECISION** : Minimum decimal precision possible for users to select.


## Technologies used

This app is developed by **React with Typescript** and below technologies are used as well:

 - **Redux toolkit**  is used for state management and handle backend API call.
 - **Open API request**  default request service is borrowed from Open API codegen as its simple, easy and open to future changes (e.g. adding auth tokens and headers).
 - **ANT design** is a UI library with great Typescript support.
 - **Styled components** is used to add styling to component. this will prevent usual mistakes during development that happens with css classes and improves reusability.
 - **ESlint** helps to keep the source code clean and more readable. 
 - **Jest** is used for component and unit testing


## Features I wanted to add but was not asked

Below ideas came to my mind during development. however, it was not asked so I would add it here:

 - Search, filter and sort coins on the table
 - Change default font, sizes and colors for various views (e.g. Desktop, mobile, etc)

