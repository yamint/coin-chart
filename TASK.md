Hello,

Many thanks for your application to WisdomTree and congratulations for making it to the next stage!

We ask that you complete the test within 7 days of receiving if that is possible. However, we can be flexible and give you some extra time to return the test should you need it.

Please find test instructions below...

Best of luck with the test, we look forward to seeing your results.


## Assignment

### "Coin Watch - live crypto prices"

Create a React application that connects to the (public)coinbase API(exchange rates) and displays live prices to the user in a tabular format.
The design and implementation choices (libraries, frameworks, ui, etc) are completely up to you as long as the application complies with the following requirements:

1. User should see live prices updating without having to refresh the page.
2. On price change, the colour of the (latest price)text should flash in red if the price went down or green if the price went up.


Note:
Please use git and commit as frequently as possible to make the review easier. 
Our preferred stack is ReactJS + redux hooks
Up to you which coins you want to show.

API reference: https://developers.coinbase.com/api/v2?shell#exchange-rates


Once completed, please push your changes to a bitbucket repository and share it with dkvochkin@wisdomtree.com and gharibabu@wisdomtree.com


Have fun!